# Django Signals for Average Rating on Recipes

1. Create your `Recipe` and `Rating` models in your Django app:

```python
from django.db import models

class Recipe(models.Model):
    # Your Recipe model fields go here

class Rating(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    score = models.PositiveIntegerField()

    # Other fields and methods for your Rating model
```

2. Create a signal handler function that calculates the average rating for a recipe and updates the `Recipe` model accordingly. You can use the `post_save` and `post_delete` signals on the `Rating` model for this:

```python
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from .models import Rating

@receiver(post_save, sender=Rating)
@receiver(post_delete, sender=Rating)
def update_recipe_average_rating(sender, instance, **kwargs):
    recipe = instance.recipe
    ratings = Rating.objects.filter(recipe=recipe)
    total_ratings = ratings.count()
    total_rating_score = sum(rating.score for rating in ratings)

    if total_ratings > 0:
        average_rating = total_rating_score / total_ratings
    else:
        average_rating = 0

    recipe.average_rating = average_rating
    recipe.save()
```

3. Ensure that your signal handler function is connected by importing it in the `apps.py` of your Django app and adding the appropriate `ready` method. For example:

```python
# myapp/apps.py
from django.apps import AppConfig


class RecipesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "recipes"

    def ready(self):
        import recipes.signals
```

4. With this setup, each time a `Rating` instance is created,  updated, or deleted the `update_recipe_average_rating` signal handler function will be called automatically. It calculates the average rating for the associated `Recipe` and updates the `average_rating` field of that `Recipe`.

This approach allows you to keep this logic separate from your model classes, making it more maintainable and modular. It's considered a good practice to use signals for such cross-model interactions in Django.
